package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectDetachTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-detach-task";
    }

    @NotNull
    @Override
    public String description() {
        return "Detach task from project.";
    }

    @Override
    public void execute() throws EmptyUserIdException_Exception, DeniedAccessException_Exception, EmptyUserException, EmptyProjectIdException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception, EmptyIdException_Exception {
        System.out.println("[LIST PROJECTS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @NotNull final SessionDTO session = serviceLocator.getSystemObjectService().getSession();
        projectEndpoint.detachTaskFromProject(session, projectId, taskId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
