package com.rencredit.jschool.boruak.taskmanager.locator;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public class EndpointLocator implements IEndpointLocator {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final IUserEndpoint userEndpoint;

    @NotNull
    private final ISessionEndpoint sessionEndpoint;

    @NotNull
    private final ITaskEndpoint taskEndpoint;

    @NotNull
    private final IProjectEndpoint projectEndpoint;

    @NotNull
    private final IAdminEndpoint adminEndpoint;

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final IAuthEndpoint authEndpoint;

    public EndpointLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        userEndpoint = new UserEndpoint(serviceLocator);
        sessionEndpoint = new SessionEndpoint(serviceLocator);
        taskEndpoint = new TaskEndpoint(serviceLocator);
        projectEndpoint = new ProjectEndpoint(serviceLocator);
        adminEndpoint = new AdminEndpoint(serviceLocator);
        adminUserEndpoint = new AdminUserEndpoint(serviceLocator);
        authEndpoint = new AuthEndpoint(serviceLocator);
    }

}
