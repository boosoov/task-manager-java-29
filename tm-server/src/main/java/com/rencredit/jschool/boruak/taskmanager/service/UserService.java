package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class UserService extends AbstractService<UserDTO> implements IUserService {

    @Override
    public void add(@Nullable final String login, @Nullable final String password) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash);
        add(login, user);
    }

    @Override
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final String firstName) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyFirstNameException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, firstName);
        add(login, user);
    }

    @Override
    public void add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyRoleException, EmptyPasswordException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, role);
        add(login, user);
    }

    public void add(@Nullable final String login, @Nullable final UserDTO user) throws BusyLoginException, EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            if (repository.findByLoginDTO(login) != null) throw new BusyLoginException();
            repository.add(user);
            repository.commit();
        } catch (final BusyLoginException e) {
            repository.rollback();
            throw new BusyLoginException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }


    @Override
    public void addUser(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash);
        addUser(login, user);
    }

    @Override
    public void addUser(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, role);
        addUser(login, user);
    }

    @Override
    public void addUser(@Nullable final String login, @Nullable final User user) throws EmptyLoginException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (user == null) throw new EmptyUserException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            if (repository.findByLoginDTO(login) != null) throw new BusyLoginException();
            repository.addUser(user);
            repository.commit();
        } catch (final BusyLoginException e) {
            repository.rollback();
            throw new BusyLoginException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO getById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IUserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.findByIdDTO(id);

        return user;
    }

    @Nullable
    @Override
    public UserDTO getByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @NotNull final IUserRepository repository = new UserRepository();
        @Nullable final UserDTO user = repository.findByLoginDTO(login);

        return user;
    }

    @Nullable
    @Override
    public UserDTO updatePasswordById(@Nullable final String id, @Nullable final String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final String passwordHash = HashUtil.getHashLine(newPassword);
            if (passwordHash == null) throw new IncorrectHashPasswordException();
            @Nullable final UserDTO user = repository.findByIdDTO(id);
            if (user == null) throw new EmptyUserException();
            user.setPasswordHash(passwordHash);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final IncorrectHashPasswordException e) {
            repository.rollback();
            throw new IncorrectHashPasswordException();
        }catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO editProfileById(@Nullable final String id, @Nullable final String firstName) throws EmptyUserException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final UserDTO user = repository.findByIdDTO(id);
            if (user == null) throw new EmptyUserException();
            user.setFirstName(firstName);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) throws EmptyUserException, EmptyLastNameException, EmptyFirstNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final UserDTO user = repository.findByIdDTO(id);
            if (user == null) throw new EmptyUserException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO editProfileById(
            @Nullable final String id,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws EmptyUserException, EmptyMiddleNameException, EmptyLastNameException, EmptyFirstNameException, EmptyEmailException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final UserDTO user = repository.findByIdDTO(id);
            if (user == null) throw new EmptyUserException();
            user.setEmail(email);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            repository.removeById(id);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            repository.removeByLogin(login);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByUser(@Nullable final UserDTO user) throws EmptyUserException {
        if (user == null) throw new EmptyUserException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            repository.removeByUser(user);
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final UserDTO user = repository.findByLoginDTO(login);
            if (user == null) throw new EmptyUserException();
            user.setLocked(true);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            @Nullable final UserDTO user = repository.findByLoginDTO(login);
            if (user == null) throw new EmptyUserException();
            user.setLocked(false);
            repository.merge(user);
            repository.commit();
            return user;
        } catch (final EmptyUserException e) {
            repository.rollback();
            throw new EmptyUserException();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
            return null;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<UserDTO> getList() {
        @NotNull final IUserRepository repository = new UserRepository();
        @NotNull final List<UserDTO> list = repository.getListDTO();
        return list;
    }

    @Override
    public void clearAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException {
        @NotNull final IUserRepository repository = new UserRepository();
        try {
            repository.begin();
            repository.clearAll();
            repository.commit();
        } catch (final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

        add("admin", "admin", Role.ADMIN);
        add("test", "test");
        add("1", "1");
    }

}
