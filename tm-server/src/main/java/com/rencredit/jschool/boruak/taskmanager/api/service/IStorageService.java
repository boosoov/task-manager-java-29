package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;

import java.io.IOException;

public interface IStorageService {

    boolean clearBase64() throws IOException;

    boolean loadBase64() throws IOException, ClassNotFoundException, EmptyElementsException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException;

    boolean saveBase64() throws IOException;

    boolean clearBinary() throws IOException;

    boolean loadBinary() throws IOException, ClassNotFoundException, EmptyElementsException;

    boolean saveBinary() throws IOException;

    boolean clearJson() throws IOException;

    boolean loadJson() throws IOException, EmptyElementsException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException;

    boolean saveJson() throws IOException;

    boolean clearXml() throws IOException;

    boolean loadXml() throws IOException, EmptyElementsException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException;

    boolean saveXml() throws IOException;

}
